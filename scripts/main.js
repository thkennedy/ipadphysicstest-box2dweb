window.onload = function(){
       
    psl = com.playstylelabs.Instance();
    var pInstance = psl;
    
    //var bgrd = document.getElementById("background");
    //var ctx = bgrd.getContext('2d');
    //var img = new Image();
    
    //img.onload = function(){
    //    ctx.setTransform(1,0,0,1,0,0);
    //    ctx.scale(0.30,0.2)
    //    ctx.drawImage(img,0,0,1024, 767);
    //    //ctx.drawImage(img, 0,0,400, 191);
    //}
    //img.src = "images/cartoonforest.jpg";
    
    
    
    ////////////////////////
    //  LOAD AUDIO
    //////////////////////
    {
    //touch = psl.Audio.Add(sKey, sPath, bLoop, bAutoplay, sType);
    
    var touch = psl.Audio.Add("touch", "sounds/touch", false, false);
    var youWin = psl.Audio.Add("youWin", "sounds/youWin", false, false);
    var tryAgain = psl.Audio.Add("tryAgain", "sounds/tryAgain", false, false);
    }  
    //////////////////////
    // WIN/LOSE IMAGES
    /////////////////////
    {
    //WIN
     
    // create div element
    var youWinDiv = pInstance.Create.HTML("div");
    youWinDiv.html.id = "YouWin";
    youWinDiv.html.style.position = "absolute";
    if(youWinDiv.html.style.webkitTransform != undefined )
        youWinDiv.html.style.webkitTransform = "translate(256px, 128px)";
    else
        youWinDiv.html.style.MozTransform = "translate(256px, 128px)";
        
    youWinDiv.html.style.background = "url(images/YouWin.png)";
    youWinDiv.html.style.width = "512px";
    youWinDiv.html.style.height = "256px";
    youWinDiv.Append.ToHTML(document.body);
    youWinDiv.html.style.zIndex = 100;
    youWinDiv.html.style.display = "none";
    
      // create div element
    var tryAgainDiv = pInstance.Create.HTML("div");
    tryAgainDiv.html.id = "YouWin";
    tryAgainDiv.html.style.position = "absolute";
    if(tryAgainDiv.html.style.webkitTransform != undefined )
        tryAgainDiv.html.style.webkitTransform = "translate(256px, 128px)";
    else
        tryAgainDiv.html.style.MozTransform = "translate(256px, 128px)";
    tryAgainDiv.html.style.background = "url(images/TryAgain.png)";
    tryAgainDiv.html.style.width = "512px";
    tryAgainDiv.html.style.height = "256px";
    tryAgainDiv.Append.ToHTML(document.body);
    tryAgainDiv.html.style.zIndex = 100;
    tryAgainDiv.html.style.display = "none";
    }
    
    // Box2D World Object
    var world;
    
    
    // this is the moving entity class for the game.
    function CEntity (Sprite, Type, Body){
        var self = this;
        
        // container for the onTouch events
        self.HTML;
        
        self.HasBody = false;
        // body object for the entity
        if (Body != null)
        {
            var _body = Body.GetBody();
            self.HasBody = true;
        }
        
        // 2D pos scratch pad var
        var _Pos2D = new Physics.b2Vec2;
        
        // which track the ent is on, this determines y value and speed
        var _row;
        
        // am I moving left or right?
        var _speed;
        
        // where ent is on the screen horizontally
        var _x;
        
        // the y or vertical position
        var _y;
        
        // which sprite am I using?
        var _Sprite = Sprite;
        
        // what am I? 0 = triangle, 1 = square, 2 = circle
        self.Type = Type;
        
        self.SetCallback = function(){
            _Sprite.graphic.html.addEventListener("touchstart", self.OnTouch, false);
            _Sprite.graphic.html.addEventListener("mousedown", self.OnTouch, false);
        }
        
        self.SetX = function(x){
            _x = x;
            
            // if a body is attached, set its x too
            if (self.HasBody)
            {
                _Pos2D.Set(x,_y);
                                
                _body.SetPosition(_Pos2D);
            }
        }
        
        self.SetY = function(y){
            _y = y;
        }
        
        // the update function
        self.Update = function(){
            
            // if body is attached, follow the body
            if (self.HasBody)
            {
                _x = _body.GetPosition().x;
                _y = _body.GetPosition().y;
                
            }
            else{
                // update the horizontal movement
                _x += _speed;
            }
            
            
            
            _Sprite.Move.ToPosition(_x,_y);
            
            
            // if I'm off screen
            if (_x > 1024 || _x < -64)
            {
                
                self.SelectNewRow(Number((Math.random()* 5).toFixed(0)) );
                
            }
            
        }
        
        self.OnTouch = function()
        {
            touch.Play();
            
            // set speed to 0
            _speed = 0;
            
            // remove touch listener
            _Sprite.graphic.html.removeEventListener("touchstart", self.OnTouch);
            _Sprite.graphic.html.removeEventListener("mousedown", self.OnTouch);
            
            // add to selection queu
            Game.SelectionQueue.AddEntity(self);
            
            
        }
        
        //_Sprite.graphic.html.addEventListener("touchstart", self.OnTouch, false);
        //_Sprite.graphic.html.addEventListener("onclick", self.OnTouch, false);
        
        self.SelectNewRow = function(rowNumber){
            switch(rowNumber)
            {
                case 0:
                    _row = 0;
                    _x = 1024;
                    _y = 180;
                    _speed = -10;
                    _Sprite.Animation.SetActive("WalkLeft");
                    
                    break;
                case 1:
                    _row = 1;
                    _x = 1024;
                    _y = 260;
                    _speed = -10;
                    _Sprite.Animation.SetActive("WalkLeft");
                    break;
                case 2:
                    _row = 2;
                    _x = -64;
                    _y = 340;
                    _speed = 10;
                    _Sprite.Animation.SetActive("WalkRight");
                    break;
                case 3:
                    _row = 3;
                    _x = 1024;
                    _y = 420;
                    _speed = -10;
                    _Sprite.Animation.SetActive("WalkLeft");
                    break;
                case 4:
                    _row = 4;
                    _x = -64;
                    _y = 500;
                    _speed = 10;
                    _Sprite.Animation.SetActive("WalkRight");
                    break;
                default:
                    _row = 4;
                    _x = -64;
                    _y = 500;
                    _speed = 10;
                    _Sprite.Animation.SetActive("WalkRight");
                    break;
            }
        }
        
    }
    
    Physics =
    {
        InitPhysics: function()
        {
            
            // defining helpful vars to have to be able to quickly create objects
            // defining the vec2
            Physics.b2Vec2 = Box2D.Common.Math.b2Vec2;
            // body characteristics
            Physics.b2BodyDef = Box2D.Dynamics.b2BodyDef;
            // polygon definition, for custom objects
            //Physics.b2PolygonDef = Box2D.Dynamics.b2PolygonDef;
            // rigid body class
            Physics.b2Body = Box2D.Dynamics.b2Body;
            // fixture characteristics
            Physics.b2FixtureDef = Box2D.Dynamics.b2FixtureDef;
            // fixture class 
            Physics.b2Fixture = Box2D.Dynamics.b2Fixture;
            // world dynamics
            Physics.b2World = Box2D.Dynamics.b2World;
            // mass data
            Physics.b2MassData = Box2D.Collision.Shapes.b2MassData;
            // Base polygon shape
            Physics.b2PolygonShape = Box2D.Collision.Shapes.b2PolygonShape;
            // base circle shape
            Physics.b2CircleShape = Box2D.Collision.Shapes.b2CircleShape;
            // ability to use canvas to draw
            Physics.b2DebugDraw = Box2D.Dynamics.b2DebugDraw;
            
            
            
            // creating the "world" in box2d
            world = new Physics.b2World(
                  new Physics.b2Vec2(0, 10)    //gravity - positive is down
               ,  true                 //allow sleep
            );
        },
            
        CreateGround: function(x,y)
        {   
            // creating a fixture - holds data for the body collision resolution
            //  can set separately, but just setting all same for now
            var fixDef = new Physics.b2FixtureDef;
            fixDef.density = 1.0;
            fixDef.friction = 0.5;
            fixDef.restitution = 0.2;
            
            
            var bodyDef = new Physics.b2BodyDef;
            
            //create ground
            bodyDef.type = Physics.b2Body.b2_staticBody;
            bodyDef.position.x = 10;
            bodyDef.position.y = 13;
            fixDef.shape = new Physics.b2PolygonShape;
            fixDef.shape.SetAsBox(8, 0.5);
            world.CreateBody(bodyDef).CreateFixture(fixDef);
        },
        
        CreateBox: function(posX,posY,halfX,halfY)
        {
            var fixDef = new Physics.b2FixtureDef;
            fixDef.density = 1.0;
            fixDef.friction = 0.5;
            fixDef.restitution = 0.2;
            
            // create new body definition
            var bodyDef = new Physics.b2BodyDef;
            
            // type of body
            bodyDef.type = Physics.b2Body.b2_dynamicBody;
            
            // create new shape
            fixDef.shape = new Physics.b2PolygonShape;
            // set it as a box, setting half extents
            fixDef.shape.SetAsBox(halfX,halfY);
            // setting x and y position
            bodyDef.position.x = posX;
            bodyDef.position.y = posY;
            // telling the world to create the body
            return world.CreateBody(bodyDef).CreateFixture(fixDef);
        },
        
        CreateCircle: function(posX, posY, radius)
        {
            
            var fixDef = new Physics.b2FixtureDef;
            fixDef.density = 1.0;
            fixDef.friction = 0.5;
            fixDef.restitution = 0.2;
            
            // create new body definition
            var bodyDef = new Physics.b2BodyDef;
            
            // type of body
            bodyDef.type = Physics.b2Body.b2_dynamicBody;
            
            // create circle shape
            fixDef.shape = new Physics.b2CircleShape( radius);
            // set x and y position
            bodyDef.position.x = posX;
            bodyDef.position.y = posY;
            // tell world to create the body
            world.CreateBody(bodyDef).CreateFixture(fixDef);
            
        },
        
        // equilateral triangle
        CreateTriangle: function(posX,posY, triScale)
        {
            
            //create simple triangle
         	var bodyDef = new Physics.b2BodyDef;
		var fixDef = new Physics.b2FixtureDef;
		fixDef.density = 1.0;
		fixDef.friction = .5;
		fixDef.restitution = 0.2;

         	bodyDef.type = Physics.b2Body.b2_dynamicBody;
	 	fixDef.shape = new Physics.b2PolygonShape;
	  	
                var scale = triScale;
 	  	
                fixDef.shape.SetAsArray([
		  new Physics.b2Vec2(scale*0.866 , scale*0.5),
		  new Physics.b2Vec2(scale*-0.866, scale*0.5),
		  new Physics.b2Vec2(0, scale*-1),
		  ]);
            	
                bodyDef.position.x = posX;
	    	bodyDef.position.y = posY;
		world.CreateBody(bodyDef).CreateFixture(fixDef);
                
                
            
        },
        CreateShapes: function()
        {
            var fixDef = new Physics.b2FixtureDef;
            fixDef.density = 1.0;
            fixDef.friction = 0.5;
            fixDef.restitution = 0.2;
            
            // create new body definition
            var bodyDef = new Physics.b2BodyDef;
        
            //create some objects
            bodyDef.type = Physics.b2Body.b2_dynamicBody;
            for(var i = 0; i < 100; ++i)
            {
               if(Math.random() > 0.5)
               {
                    Physics.CreateTriangle(Math.random() * 10, Math.random() * 10, Math.random() + .1);
                    //Physics.CreateBox(Math.random() * 10, Math.random() * 10,
                    //          Math.random() +.1, Math.random() + .1);
                  //fixDef.shape = new Physics.b2PolygonShape;
                  //fixDef.shape.SetAsBox(
                  //      Math.random() + 0.1 //half width
                  //   ,  Math.random() + 0.1 //half height
                  //);
                  
               }
               else
               {
                    //Physics.CreateTriangle(Math.random() * 10, Math.random() * 10, 2);
                    Physics.CreateCircle(Math.random() * 10, Math.random() * 10, Math.random() + .1);
                  //fixDef.shape = new Physics.b2CircleShape(
                  //   Math.random() + 0.1 //radius
                  //);
               }
               //bodyDef.position.x = Math.random() * 10;
               //bodyDef.position.y = Math.random() * 10;
               //world.CreateBody(bodyDef).CreateFixture(fixDef);
            }
        },
        
        DebugDraw: function()
        {
            
            //setup debug draw - with no graphics sys attached, this is box2d's drawing system
            var debugDraw = new Physics.b2DebugDraw();
                           debugDraw.SetSprite(document.getElementById("background").getContext("2d"));
                           debugDraw.SetDrawScale(30.0);
                           debugDraw.SetFillAlpha(0.3);
                           debugDraw.SetLineThickness(1.0);
                           debugDraw.SetFlags(Physics.b2DebugDraw.e_shapeBit | Physics.b2DebugDraw.e_jointBit);
                           world.SetDebugDraw(debugDraw);
        },
        Run: function()
        {
            
            // has window refresh 60fps, calls update over and over. 
            window.setInterval(update, 1000 / 60);
            
            function update()
            {
                world.Step(
                      1 / 60   //frame-rate
                   ,  10       //velocity iterations
                   ,  10       //position iterations
                );
                world.DrawDebugData();
                world.ClearForces();
            };
        }
             
             
            
        
        
        
    }

     Game = {
        
        Sprites: [],
        Entities: [],
        
        // sprite sheets
        ss: null,
        ss2: null,
        ss3: null,
                
        // list of animations
        SpriteAnimations: [],
        
        // scratch pad vars
        x: 0,
        y : 0,
        z : 0,
        
    
        InitAssets : function()
        {
            //
            //  Build Sprite Sheet
            //
            
            // create and init sprite sheets
            // walking triangle
            Game.ss = psl.Create.SpriteSheet("triangle");
            Game.ss.cellWidth = 66;
            Game.ss.cellHeight = 66;
            Game.ss.rows = 2;
            Game.ss.columns = 5;
            Game.ss.width = 328;
            Game.ss.height = 130;
            Game.ss.path = "../images/trianglesheet.png";
            Game.ss.BuildClass();
            
            // walking square
            Game.ss2 = psl.Create.SpriteSheet("square");
            Game.ss2.cellWidth = 66;
            Game.ss2.cellHeight = 66;
            Game.ss2.rows = 2;
            Game.ss2.columns = 5;
            Game.ss2.width = 328;
            Game.ss2.height = 130;
            Game.ss2.path = "../images/squaresheet.png";
            Game.ss2.BuildClass();
            
            // walking square
            Game.ss3 = psl.Create.SpriteSheet("circle");
            Game.ss3.cellWidth = 66;
            Game.ss3.cellHeight = 66;
            Game.ss3.rows = 2;
            Game.ss3.columns = 5;
            Game.ss3.width = 328;
            Game.ss3.height = 130;
            Game.ss3.path = "../images/circlesheet.png";
            Game.ss3.BuildClass();
            
            // building animations
            
            // triangle walking left
            Game.SpriteAnimations.push(psl.Create.Animation("WalkLeft", 5,0,0,Game.ss) );
            
            // triangle walking right
            Game.SpriteAnimations.push(psl.Create.Animation("WalkRight", 5,1,0,Game.ss) );
            
            // square walking left
            Game.SpriteAnimations.push(psl.Create.Animation("WalkLeft", 5,0,0,Game.ss2) );
             
            // square walking right
            Game.SpriteAnimations.push(psl.Create.Animation("WalkRight", 5,1,0,Game.ss2) );
            
            // circle walking left
            Game.SpriteAnimations.push(psl.Create.Animation("WalkLeft", 5,0,0,Game.ss3) );
            
            // circle walking right
            Game.SpriteAnimations.push(psl.Create.Animation("WalkRight", 5,1,0,Game.ss3) );
            
               
            //
            //  Build Sprite
            //
            // array of all sprites -
            
            //TriangleSprite = Game.CreateTriangle(0,0,0,Game.SpriteAnimations[0],Game.SpriteAnimations[1]);
            //SquareSprite = Game.CreateSquare(0,0,0,Game.SpriteAnimations[2],Game.SpriteAnimations[3]);
            //CircleSprite = Game.CreateCircle(0,0,0, Game.SpriteAnimations[4], Game.SpriteAnimations[5]);
            
        },
                
        
               
        
        CreateTriangle : function(x,y,direction,leftAnimation, rightAnimation){
             
            var sprite = psl.Create.Sprite("Triangle");
            sprite.Animation.Load(leftAnimation);
            sprite.Animation.Load(rightAnimation);
            
            // if direction is 0, go left, if direction is 1, go right
            direction < 1 ? sprite.Animation.SetActive("WalkLeft") : sprite.Animation.SetActive("WalkRight");
            
            sprite.Animation.SetFrame(Number((Math.random()* 2).toFixed(0)));
            sprite.Move.ToPosition(x,y);
            sprite.graphic.Append.ToHTML(document.body);
            
            //Game.Sprites.push(sprite);
            
            return sprite;
        },
        
        CreateSquare : function(x,y,direction,leftAnimation, rightAnimation){
             
            var sprite = psl.Create.Sprite("Square");
            sprite.Animation.Load(leftAnimation);
            sprite.Animation.Load(rightAnimation);
            
            // if direction is 0, go left, if direction is 1, go right
            direction < 1 ? sprite.Animation.SetActive("WalkLeft") : sprite.Animation.SetActive("WalkRight");
            
            sprite.Animation.SetFrame(Number((Math.random()* 2).toFixed(0)));
            sprite.Move.ToPosition(x,y);
            sprite.graphic.Append.ToHTML(document.body);
            
            //Game.Sprites.push(sprite);
                    
            return sprite;
        },
        
        CreateCircle : function(x,y,direction,leftAnimation, rightAnimation){
             
            var sprite = psl.Create.Sprite("Circle");
            sprite.Animation.Load(leftAnimation);
            sprite.Animation.Load(rightAnimation);
            
                  
           // if direction is 0, go left, if direction is 1, go right
            direction < 1 ? sprite.Animation.SetActive("WalkLeft") : sprite.Animation.SetActive("WalkRight");
            
            sprite.Animation.SetFrame(Number((Math.random()* 2).toFixed(0)));
            sprite.Move.ToPosition(x,y);
            sprite.graphic.Append.ToHTML(document.body);
            
            //Game.Sprites.push(sprite);
            
            return sprite;
        },
        
        GameObjectives : {
            
            // currently selected objective
            _Objective : 999,
            
            // list of objective sprites
            ObjectiveSprites : [],
            
            // number of objectives, used to dynamically allow more objectives to be added later
            NumObjectives : 0,
            
            
            
            
                        
            // resets objectives
            InitObjectives : function(){
                
                // allocate memory for objectives
                Game.GameObjectives.ObjectiveSprites = [];
                
                
                // create all sets of objectives, place offscreen
                // tri
                Game.GameObjectives.ObjectiveSprites.push(Game.CreateTriangle(-350,0,1,Game.SpriteAnimations[0],Game.SpriteAnimations[1]));
                Game.GameObjectives.ObjectiveSprites.push(Game.CreateTriangle(-450,0,1,Game.SpriteAnimations[0],Game.SpriteAnimations[1]));
                Game.GameObjectives.ObjectiveSprites.push(Game.CreateTriangle(-550,0,1,Game.SpriteAnimations[0],Game.SpriteAnimations[1]));
                
                // sqr
                Game.GameObjectives.ObjectiveSprites.push(Game.CreateSquare(-350,0,1,Game.SpriteAnimations[2],Game.SpriteAnimations[3]));
                Game.GameObjectives.ObjectiveSprites.push(Game.CreateSquare(-450,0,1,Game.SpriteAnimations[2],Game.SpriteAnimations[3]));
                Game.GameObjectives.ObjectiveSprites.push(Game.CreateSquare(-550,0,1,Game.SpriteAnimations[2],Game.SpriteAnimations[3]));
                
                // cir
                Game.GameObjectives.ObjectiveSprites.push(Game.CreateCircle(-350,0,1,Game.SpriteAnimations[4],Game.SpriteAnimations[5]));
                Game.GameObjectives.ObjectiveSprites.push(Game.CreateCircle(-450,0,1,Game.SpriteAnimations[4],Game.SpriteAnimations[5]));
                Game.GameObjectives.ObjectiveSprites.push(Game.CreateCircle(-550,0,1,Game.SpriteAnimations[4],Game.SpriteAnimations[5]));
                
                // set number of objectives
                NumObjectives = Game.GameObjectives.ObjectiveSprites.length/3;
                
                
            },
            
            NewGame : function(){
                
                // move all objective sprites offscreen
                for ( i = Game.GameObjectives.ObjectiveSprites.length-1; i >= 0; --i)
                {
                    Game.GameObjectives.ObjectiveSprites[i].Move.ToPosition(-500,0,1);
                   
                }
                // select shape - 0 = triangle, 1 = square, 2 = circle
                // random roll for objective choice
                Game.GameObjectives._Objective = Number((Math.random()* (NumObjectives-1)).toFixed(0));
                
                // setting scratch var for y placement
                Game.y = 10;
                
                // move selected objective onto screen
                Game.GameObjectives.ObjectiveSprites[Game.GameObjectives._Objective*3].Move.ToPosition(350,Game.y,1);
                Game.GameObjectives.ObjectiveSprites[Game.GameObjectives._Objective*3+1].Move.ToPosition(450,Game.y,1);
                Game.GameObjectives.ObjectiveSprites[Game.GameObjectives._Objective*3+2].Move.ToPosition(550,Game.y,1);
                
            }
          
          
        },
        
        // this object holds the entities that are selected
        SelectionQueue : {
            // array holding entities
            _EntityQueue : [],
            
            // length of selection queue
            QueueLength: -1,
            
            // array that holds the results I check through
            ResultsList: [],
            
            Init : function(){
                Game.SelectionQueue._EntityQueue = [];
                Game.SelectionQueue.ResultsList = [];
            },
            
            // adds an entity to the selection queue
            AddEntity : function(Entity){
              // add the object to the selection queue
              Game.SelectionQueue._EntityQueue.push(Entity);
              
              // based on the array's new length, set the x and y values
              Game.SelectionQueue.QueueLength = Game.SelectionQueue._EntityQueue.length;

              //   adding to results list
              if (Entity.Type == Game.GameObjectives._Objective)
                  Game.SelectionQueue.ResultsList.push(1);
              else
                  Game.SelectionQueue.ResultsList.push(0);
              
              
              
              // set the y value to 580
              Game.SelectionQueue._EntityQueue[Game.SelectionQueue.QueueLength-1].SetY(580);
              
              
              switch(Game.SelectionQueue.QueueLength)
              {
                case 0:
                    
                    break;
                case 1:
                    Game.SelectionQueue._EntityQueue[Game.SelectionQueue.QueueLength-1].SetX(30);
                    break;
                case 2:
                    Game.SelectionQueue._EntityQueue[Game.SelectionQueue.QueueLength-1].SetX(110);
                    break;
                case 3:
                    Game.SelectionQueue._EntityQueue[Game.SelectionQueue.QueueLength-1].SetX(190);
                    break;
                case 4:
                    Game.SelectionQueue._EntityQueue[Game.SelectionQueue.QueueLength-1].SetX(270);
                    break;
                case 5:
                    Game.SelectionQueue._EntityQueue[Game.SelectionQueue.QueueLength-1].SetX(350);
                    break;
                case 6:
                    Game.SelectionQueue._EntityQueue[Game.SelectionQueue.QueueLength-1].SetX(430);
                    break;
                case 7:
                    Game.SelectionQueue._EntityQueue[Game.SelectionQueue.QueueLength-1].SetX(510);
                    break;
                case 8:
                    Game.SelectionQueue._EntityQueue[Game.SelectionQueue.QueueLength-1].SetX(590);
                    break;
                case 9:
                   Game.SelectionQueue._EntityQueue[Game.SelectionQueue.QueueLength-1].SetX(670);
                    break;
                case 10:
                    Game.SelectionQueue._EntityQueue[Game.SelectionQueue.QueueLength-1].SetX(750);
                    break;
                case 11:
                    Game.SelectionQueue._EntityQueue[Game.SelectionQueue.QueueLength-1].SetX(830);
                    break;
                case 12:
                    Game.SelectionQueue._EntityQueue[Game.SelectionQueue.QueueLength-1].SetX(910);
                    break;
                default:
                    Game.SelectionQueue._EntityQueue[Game.SelectionQueue.QueueLength-1].SetX(-64);
              }
              
                // see if the player won
                               
                if (Game.SelectionQueue.ResultsList.length > 2)
                {
                    for (Game.z = 0; Game.z < Game.SelectionQueue.ResultsList.length-2; ++Game.z )
                    {
                        if (Game.SelectionQueue.ResultsList[Game.z] == 1 &&
                            Game.SelectionQueue.ResultsList[Game.z+1] == 1 &&
                            Game.SelectionQueue.ResultsList[Game.z+2] == 1)
                        {
                            youWin.Play();
                            
                            // display win message
                            youWinDiv.html.style.display = "block";
                            // does init of game in 3 seconds
                            window.setTimeout(function(){
                                
                            // game win
                            Game.InitGame();
                            Game.GameObjectives.NewGame();
                            
                            return;}, 3000);
                            
                            // so you can't win and lose at the same time.
                            return;
                        }
                       
                       
                    }
                    
                    
                
                    // if the length is 12, quit and start new game
                    if(Game.SelectionQueue.QueueLength > 11){
                        tryAgain.Play();
                        
                        // game lost
                        // display try again image
                        tryAgainDiv.html.style.display = "block";                        
                                                
                        // re-init the game after 3 seconds
                        window.setTimeout(function(){
                            
                        // game lost
                        
                        Game.InitGame();
                        Game.GameObjectives.NewGame();
                        return;}, 3000);
                  
                    }
                
                }
              
              
                
            }
        },
        
        Clear : function(){
            // clears out old data
            
            // clear selection Queue
            Game.SelectionQueue.Init();//new Array;
            
            
            
            // clear active ents
            //Game._ActiveEntities = [];//new Array;
            
            // clear sprites
            //Game.ClearAnimation();
                
                
                
        },
        
        CreateEntities : function(){
    
            for (Game.x= 11; Game.x >=0 ; --Game.x)
            {   
                // random roll for track choice
                randNum = Number((Math.random()* 4).toFixed(0));
                
                switch(randNum)
                {
                    case 0:
                        Game.Entities[Game.x].SelectNewRow(0);
                        break;
                    case 1:
                        Game.Entities[Game.x].SelectNewRow(1);
                        break;
                    case 2:
                        Game.Entities[Game.x].SelectNewRow(2);
                        break;
                    case 3:
                        Game.Entities[Game.x].SelectNewRow(3);
                        break;
                    case 4:
                        Game.Entities[Game.x].SelectNewRow(4);
                        break;
                    default:
                        Game.Entities[Game.x].SelectNewRow(4);
                }
                
                switch(Game.x)
                {
                    case 0:
                        Game.Entities[Game.x].SetX(30);
                        break;
                    case 1:
                        Game.Entities[Game.x].SetX(510);
                        break;
                    case 2:
                        Game.Entities[Game.x].SetX(590);
                        break;
                    case 3:
                        Game.Entities[Game.x].SetX(270);
                        break;
                    case 4:
                        Game.Entities[Game.x].SetX(350);
                        break;
                    case 5:
                        Game.Entities[Game.x].SetX(190);
                        break;
                    case 6:
                        Game.Entities[Game.x].SetX(110);
                        break;
                    case 7:
                        Game.Entities[Game.x].SetX(430);
                        break;
                    case 8:
                        Game.Entities[Game.x].SetX(670);
                        break;
                    case 9:
                        Game.Entities[Game.x].SetX(750);
                        break;
                    case 10:
                        Game.Entities[Game.x].SetX(830);
                        break;
                    case 11:
                        Game.Entities[Game.x].SetX(910);
                        break;
                    default:
                        Game.Entities[Game.x].SetX(0);
                }
                
                Game.Entities[Game.x].SetCallback();
            }
            
            Game._ActiveEntities = Game.Entities.slice();                                 
             
            for(Game.x = Game._ActiveEntities.length-1; Game.x >= 0; --Game.x){
                if(Game._ActiveEntities[Game.x].speed == 0){
                    var b = 0;
                }
            }
        },

        
        InitGame : function(){
            
            // make both win and lose images not visible
            youWinDiv.html.style.display = "none";
            tryAgainDiv.html.style.display = "none";
            
            Game.Clear();
            Game.CreateEntities();
            Game.GameObjectives.NewGame();
            //_CreateDivs();
        },
        
        
        
        Update : function(){
            
            for (Game.x = Game._ActiveEntities.length-1;  Game.x >= 0; --Game.x)
            {
                Game._ActiveEntities[Game.x].Update();
            }
            
            // update animations
            for(Game.x = Game.Sprites.length; Game.x--;){
                Game.Sprites[Game.x].Animation.NextFrame();
            }
            
            for(Game.x = Game.GameObjectives.ObjectiveSprites.length; Game.x--;){
                Game.GameObjectives.ObjectiveSprites[Game.x].Animation.NextFrame();
            }
            
            
        },
        GenerateEntities: function(){
            // temporary containers for randomly chosen number and sprite
            var randNum;
            var index = 0; 
            var tempSprite;
            var lastUpdate = 0;
            var tempSprite = null;
            for (var i = 0; i < 12; i++)
            {
                
               // random roll for sprite choice
               //randNum = Number((Math.random()* 2).toFixed(0));
                
               switch(i)
                {
                    case 0:case 1: case 2: case 3:
                        //tempSprite = CreateTriangle(0,0,0);
                        Game.Sprites.push(Game.CreateTriangle(0,0,0,Game.SpriteAnimations[0],Game.SpriteAnimations[1]));
                        // create the entity
                        Game.Entities.push(new CEntity(Game.Sprites[Game.Sprites.length - 1],0));
                        break;
                    case 4:case 5:case 6:case 7:
                        //tempSprite = CreateSquare(0,0,0);
                        Game.Sprites.push(Game.CreateSquare(0,0,0,Game.SpriteAnimations[2],Game.SpriteAnimations[3]) );
                        // create the entity
                        Game.Entities.push(new CEntity(Game.Sprites[Game.Sprites.length - 1],1, Physics.CreateBox(10,10,1,1)));
                        break;
                    case 8:case 9:case 10:case 11:
                        //tempSprite = CreateCircle(0,0,0);
                        Game.Sprites.push(Game.CreateCircle(0,0,0,Game.SpriteAnimations[4],Game.SpriteAnimations[5]));
                        // create the entity
                        Game.Entities.push(new CEntity(Game.Sprites[Game.Sprites.length - 1],2));
                        break;
                    default:
                        //tempSprite = null; 
                }
                //Game.Sprites
                //Game.Game.Sprites.push(tempSprite);
                
                
            }
                
        },
        CreateDivs: function(){
            
            // creating css box for objective window
            psl.Create.CSSClass('.Objective', 'background-color: gray');
            document.getElementById("Objective").className = "Objective";
            //document.getElementById("Objective").style.position = "fixed";
            //document.getElementById("Objective").style.top = "0px";
            //document.getElementById("Objective").style.left = "250px";
            //document.getElementById("Objective").style.width = "500px";
            //document.getElementById("Objective").style.height = "80px";
            
            psl.Create.CSSClass('.SelectionOutline', 'background-color: black');
            document.getElementById("SelectionOutline").className = "SelectionOutline";
            
            psl.Create.CSSClass('.SelectionBox', 'background-color: white');
            document.getElementById("SelectionBox").className = "SelectionBox";
            
            
            
            
        }
        
        
    }
    
    
   
    /////////////////
    // GAME SETUP
    ////////////////
    
    // init physics before anything else
    Physics.InitPhysics();
    
    // single use methods
    // create all sprites and assets
    Game.InitAssets();
    // create game entities
    Game.GenerateEntities();
    // create html5 elements
    Game.CreateDivs();
    // creates game objectives, places offscreen
    Game.GameObjectives.InitObjectives();
    
    //initialize game vars & logic    
    Game.InitGame();
    
    
    Physics.CreateGround();
    Physics.CreateShapes();
    Physics.DebugDraw();
    Physics.Run();
    
    
    
    // main game update loop
    var lastUpdate = 0;
    var time;
    (Update = function(){
        time = psl.Get.Time()
        if((time - lastUpdate) > 80){
            lastUpdate = time;
            
            // update game ents
            Game.Update();
            
            
        }
        window.requestAnimFrame(Update);
        //window.webkitRequestAnimationFrame(Update);
    })();
    
    
    
    
}

//http://phrogz.net/tmp/image_move_sprites_css.html